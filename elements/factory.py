from elements.double import Double
from elements.triple import Triple
from elements.car import Car

def get_element(name):
	"""Factory for the Element class.
	"""
	return {
		'double': Double,
		'triple': Triple,
		'car': Car,
	}[name]()