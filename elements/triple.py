class Triple:
	"""Simple triple criterias class for tests.
	"""
	units = {
        'criteria_1': 'nn',
        'criteria_2': 'nn',
        'criteria_3': 'nn',
    }
	should_maximize = {
        'criteria_1': False,
        'criteria_2': True,
        'criteria_3': False,
    }
