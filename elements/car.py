class Car:
	"""Car element class, is the main multi-criterias example.
	"""
	units = {
        'horsepower':   'ch',
        'couple':       'Nm',
        'weight':       'kg',
        'acceleration': 's',
        'cost':         'euros',
        'pollution':    'g/km',
        'appearence':   '/5',
        'chassis':      '/5',
    }
	should_maximize = {
        'horsepower':   True,
        'couple':       True,
        'weight':       False,
        'acceleration': False,
        'cost':         False,
        'pollution':    False,
        'appearence':   True,
        'chassis':      True,
    }
