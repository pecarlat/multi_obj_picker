class Double:
	"""Simple double criterias class for tests.
	"""
	units = {
        'criteria_1': 'nn',
        'criteria_2': 'nn',
    }
	should_maximize = {
        'criteria_1': False,
        'criteria_2': True,
    }
