#!/usr/bin/env python3

"""Code to pick an element amongst a list using multi-objectives optinization.
"""

import argparse
from itertools import combinations, product
import inquirer

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Rectangle

from elements.factory import get_element


# Global variables
ESPI = 0.00001
VISUALIZE = True
OPTIMIZE_CRITERIA = True


def get_arguments():
    """Get command-line arguments.
    """
    parser = argparse.ArgumentParser()

    # Required arguments
    parser.add_argument('data_csv', help='CSV file with the elements to pick.')
    parser.add_argument('element', help='Name of the element class to use.')

    return parser.parse_args()

def print_element(ec, e):
    """Display method for element.
    """
    for col, item in e.iteritems():
        print(col, item * (-1 if ec.should_maximize[col] else 1))

def normalize(element, df):
    """Everything should be minimizable, normalize by -1 if maximizable.
    """
    for col in df.columns:
        df[col] = df[col] * (-1 if element.should_maximize[col] else 1)
    return df

def masked(df, mask):
    """Returns the dataset without the already processed elements.
    """
    return df[np.logical_not(mask)]

def compute_ideal(mask, df):
    """Update the ideal point's position.
    """
    return np.amin(masked(df, mask), axis=0)

def compute_nadir(ideal, mask, df):
    """Update the estimation of Nadir's point position.
    """
    filtered = masked(df, mask)
    tab = []
    for name, min_val in ideal.iteritems():
        tab.append(np.amax(filtered[filtered[name] == min_val], axis=0))
    return np.amax(tab, axis=0)

def update_mask(nadir, mask, df):
    """Update the mask with the nadir point.
    """
    return np.logical_or(mask, np.amin(np.subtract(nadir.T, df), axis=1) < 0)

def pick_best(mask, df, ideal, nadir):
    """Use Tchebychev to find the next best element.
    """
    omega = 1. / (nadir - ideal)
    omega = omega.replace([np.inf, -np.inf], 0)
    tmp = np.multiply(np.subtract(masked(df, mask), ideal), omega)
    dist_tchebychev = np.amax(tmp, axis=1) + ESPI * np.sum(tmp, axis=1)
    return np.argmin(dist_tchebychev)

def propose(element_class, element):
    """Interface to propose the current best element.
    """
    print_element(element_class, element)
    return input("OK? (Y/n)\n>> ") != 'n'

def pick_criteria_to_optimize(criterias):
    """Pick, amongst a list of criterias, the one you want to optimize.
    """
    question = [
        inquirer.List('criteria',
                      message='Which criteria do you want to improve?',
                      choices=criterias,
        ),
    ]
    #return criterias.index(inquirer.prompt(question)['criteria'])
    return inquirer.prompt(question)['criteria']

def show_graph(df, colors, ideal, nadir):
    """Show the graph if 2D or 3D.
    """
    X = df.iloc[:,0].values
    Y = df.iloc[:,1].values
    d = [y - x for x, y in zip(ideal, nadir)]
    if df.shape[1] == 2:
        plt.scatter(X, Y, c=colors.values)
        ax = plt.gca()
        ax.add_patch(Rectangle(ideal, d[0], d[1],
                               edgecolor='b', facecolor="none"))
    elif df.shape[1] == 3:
        Z = df.iloc[:,2].values
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for s, e in combinations(np.array(list(product(*zip(ideal, d)))), 2):
            if np.sum(s - e) == np.max(s - e):
                ax.plot3D(*zip(s, e), color="b")
        ax.scatter(X, Y, Z, c=colors.values)
    plt.show()

def main():
    # Parse and get arguments
    args = get_arguments()

    # Get the class of the element to use
    element = get_element(args.element)

    # Read data
    df = pd.read_csv(args.data_csv, index_col='name')
    df = normalize(element, df)
    nb_elements, nb_criterias = df.shape

    # Variables for exploration
    mask = np.zeros(nb_elements, dtype=bool)
    ideal = compute_ideal(mask, df)
    nadir = compute_nadir(ideal, mask, df)
    mask = update_mask(nadir, mask, df)
    colors = pd.Series(['blue'] * nb_elements, index=df.index)

    while not all(mask):
        best_ix = pick_best(mask, df, ideal, nadir)
        best = masked(df, mask).iloc[best_ix]
        colors[best.name] = 'red'

        if VISUALIZE:
            show_graph(df, colors, nadir, ideal)

        if propose(element, best):
            print('All good!')
            break

        if OPTIMIZE_CRITERIA:
            preference = pick_criteria_to_optimize(list(df.columns))
            nadir[list(df.columns).index(preference)] = best[preference]
            mask = update_mask(nadir, mask, df)

        mask[[i for i, _ in df.iterrows() if not mask[i]][best_ix]] = True
        colors[best.name] = 'black'

if __name__ == '__main__':
    main()
