# Multi-objectives picker
Algorithm using Nadir and Ideal points to get the best element amongst a list,
given their x caracteristics (multi-dimensional). The CRITERIA_OPTIMIZE option
updates the Nadir point given the preferences of the user.

## Use
```python
python3 basic_picker data/cars.csv car
```


## Elements
You can create your own Element class in the elements directory, to provide the
'maximization' and 'unit' settings. You also can test the double and triple
elements to visualize how the multi-criterias optimizer works.


## Visualization
![Two criterias example](double_criterias.png)